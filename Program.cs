﻿using System;

class Program2
{
    static void Main()
    {
        // Definir matrices
        int[,] matrizA = { { 1, 2 }, { 3, 4} };
        int[,] matrizB = { { 7, 8, 9 }, { 10, 11, 12 } };

        // Obtener dimensiones de las matrices
        int filasA = matrizA.GetLength(0);
        int columnasA = matrizA.GetLength(1);
        int filasB = matrizB.GetLength(0);
        int columnasB = matrizB.GetLength(1);

        // Verificar si las matrices son multiplicables
        if (columnasA != filasB)
        {
            Console.WriteLine("No se pueden multiplicar las matrices. Las columnas de la primera matriz deben ser iguales a las filas de la segunda matriz.");
            return;
        }

        // Inicializar una nueva matriz para el resultado
        int[,] resultado = new int[filasA, columnasB];

        // Realizar la multiplicación de matrices
        for (int i = 0; i < filasA; i++)
        {
            for (int j = 0; j < columnasB; j++)
            {
                for (int k = 0; k < columnasA; k++)
                {
                    resultado[i, j] += matrizA[i, k] * matrizB[k, j];
                }
            }
        }

        // Mostrar el resultado
        Console.WriteLine("Resultado de la multiplicación de matrices:");
        for (int i = 0; i < filasA; i++)
        {
            for (int j = 0; j < columnasB; j++)
            {
                Console.Write(resultado[i, j] + " ");
            }
            Console.WriteLine();
        }
    }
}
